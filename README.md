# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://JanPelicon@bitbucket.org/JanPelicon/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/JanPelicon/stroboskop/commits/62308d6060a75650f0fa52862ce4327f41dca515

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/JanPelicon/stroboskop/commits/2c26a999475e42a4c620aa2018e7569600da7bd3

Naloga 6.3.2:
https://bitbucket.org/JanPelicon/stroboskop/commits/dd0f6c8227fefce3bf187615d669747ca223442e

Naloga 6.3.3:
https://bitbucket.org/JanPelicon/stroboskop/commits/6f79bea41e881215807b8d69ade6fccb2ff141df

Naloga 6.3.4:
https://bitbucket.org/JanPelicon/stroboskop/commits/b9dea4810deac6b0005403349a2cc86ab0ccefd0

Naloga 6.3.5:

git checkout master
git merge --no-ff izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/JanPelicon/stroboskop/commits/8883cac698332d868832460e0ff48ad9c6f0ee25

Naloga 6.4.2:
https://bitbucket.org/JanPelicon/stroboskop/commits/8eceab96f4461b6e6d397b084878da8ce2ade595

Naloga 6.4.3:
https://bitbucket.org/JanPelicon/stroboskop/commits/cb3816df7bb522bb8b9c84d646a2bd0b519310ff

Naloga 6.4.4:
https://bitbucket.org/JanPelicon/stroboskop/commits/a44b43438c568e79474b3a675ba556dbbb0a45f0